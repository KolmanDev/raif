import { Module } from "@nestjs/common";
import { DatabaseModule } from "./database/DatabaseModule";
import { LoggerModule } from "./services/logger/Logger.module";
import { AccountsModule } from "./logic/accounts/Accounts.module";
import { TransferModule } from "./logic/transfer/Transfer.module";

@Module({
  imports: [
    DatabaseModule,
    LoggerModule,
    AccountsModule,
    TransferModule,
  ]
})
export class AppModule {}