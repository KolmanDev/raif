import { Dialect } from 'sequelize';

const defaults = {
  dialect:      'postgres' as Dialect,
  host:         process.env.DB_HOST || 'localhost',
  port:         parseInt(process.env.DB_PORT, 10) || 5432,
  username:     process.env.DB_USER || 'test_user',
  password:     process.env.DB_PASSWORD || 'test_password',
  database:     process.env.DB_NAME || 'test',
  synchronize:  false,
  logging:      false,
  pool: {
    max:        20,
  },
};

const config = {
  development: {
    ...defaults,
  },
  production: {
    ...defaults,
  },
  test: {
    ...defaults,
  }
}

const env = process.env.NODE_ENV || 'development';

// export default config[env];

/**
 * Эта строка нужна, чтоб миграции сработали
 * https://stackoverflow.com/questions/46694157/dialect-needs-to-be-explicitly-supplied-as-of-v4-0-0
 */
module.exports = config[env];
