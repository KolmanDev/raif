import { Injectable } from '@nestjs/common';

@Injectable()
export class LoggerService {
  error(message: any) { console.error(message); }

  log(message: any) { console.log(message); }

  warn(message: any) { console.warn(message); }

  info(message: any) { console.info(message); }

  trace(message: any) { console.trace(message); }
}