import { Model, Table, Column, DataType } from "sequelize-typescript";

@Table({
  tableName: 'transactions',
  createdAt: true,
  updatedAt: false,
  indexes: [
    {
      name: 'transactions_accounts',
      unique: false,
      fields: ['account_id'],
    }
  ]
})
export class Transactions extends Model {
  /** Айдишник транзакции */
  @Column({
    type: DataType.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  })
    id: number;

  /** Айдишник аккаунта, у которого меняется баланс */
  @Column({
    type: DataType.BIGINT,
    allowNull: false,
  })
    account_id: number;

  /** Сумма транзакции */
  @Column({
    type: DataType.DECIMAL(12, 2),
    allowNull: false,
  })
    amount: number;

  /** Валюта транзакции */
  @Column({
    type: DataType.CHAR(3),
    allowNull: false,
    defaultValue: 'RUB'
  })
    currency: string;

  /** Дата транзакции */
  @Column({
    allowNull: false,
    type: DataType.DATE,
    defaultValue: DataType.NOW,
  })
  createdAt: Date;
}

/**
 * Запись транзакции по аккаунту 
 * @param id айдишник транзакции
 * @param account_id айдишник аккаунта
 * @param amount сумма транзакции
 * @param currency валюта транзакции
 * @param createdAt дата транзакции
*/
export type TTransaction = {
  id: number;

  account_id: number;

  amount: number;

  currency: string;

  createdAt: Date;
}