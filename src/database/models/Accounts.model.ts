import {
  Column, DataType, Model, Table,
} from 'sequelize-typescript';

@Table({
  tableName: 'accounts',
  createdAt: false,
  updatedAt: false,
})
export class Accounts extends Model {
  /** Идентификатор аккаунта */
  @Column({
    type: DataType.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  })
    account_id: number;

  /** Баланс аккаунта */
  @Column({
    type: DataType.DECIMAL(12, 2),
    allowNull: false,
    defaultValue: 0,
  })
    balance: number;

  /** Валюта баланса */
  @Column({
    type: DataType.CHAR(3),
    allowNull: false,
    defaultValue: 'RUB'
  })
    currency: string;
}

/**
 * Баланс аккаунта
 * @param account_id айдишник аккаунта
 * @param balance баланс аккаунта
 * @param currency валюта баланса
*/
export type TAccount = {
  account_id: number,

  balance: number,

  currency: string,
}
