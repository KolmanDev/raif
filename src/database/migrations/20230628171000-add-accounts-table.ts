import { QueryInterface, DataTypes, Op } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface) => 
    queryInterface.createTable('accounts', {
      account_id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },

      balance: {
        type: DataTypes.DECIMAL(12, 2),
        allowNull: false,
        defaultValue: 0,
      },

      currency: {
        type: DataTypes.CHAR(3),
        allowNull: false,
        defaultValue: 'RUB'
      },
    })
    .then(() => 
      queryInterface.addConstraint('accounts', {
        fields: ['balance'],
        type: 'check',
        where: {
          balance: {
            [Op.gte]: 0
          }
        }
      })
    ),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('accounts'),
};
