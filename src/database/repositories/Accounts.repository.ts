import { Injectable, BadRequestException, InternalServerErrorException, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Accounts, TAccount, Transactions } from '../models';
import { Sequelize } from 'sequelize-typescript';
import ERRORS from '../../errors';

@Injectable()
export class AccountsRepository {
  constructor(
    @InjectModel(Accounts)
    private readonly accountsModel: typeof Accounts,

    @InjectModel(Transactions)
    private readonly transactionsModel: typeof Transactions,

    private readonly sequelize: Sequelize,
  ) {}

  /** Поиск баланса по аккаунту */
  get(
    account_id: number,
    transaction?: any,
  ): Promise<TAccount> {
    return this.accountsModel.findByPk(account_id, {
      raw: true,
      transaction,
    });
  }

  /** Создание счета */
  create(
    currency: string,
  ): Promise<TAccount> {
    return this.accountsModel.create({ currency });
  }

  /** Трансфер с баланса одного аккаунта на другой */
  async transferBetweenAccounts(
    sourceAccountId: number,
    targetAccountId: number,
    amount: number,
    currency: string
  ) {
    const transaction = await this.sequelize.transaction();
    
    try {
      /** Проверяем, что у аккаунта-источника достаточно средств */
      const { balance } = await this.get(sourceAccountId, transaction);

      if (balance < amount) {
        throw new BadRequestException(ERRORS.ACCOUNT.NO_ENOUGH_MONEY.description);
      }

      await Promise.all([
        /** Снятие с аккаунта - источника */
        this._updateBalance(sourceAccountId, -amount, currency, transaction),

        /** Начисление целевому аккаунту */
        this._updateBalance(targetAccountId, amount, currency, transaction),
      ]);

      await transaction.commit();

    } catch (error) {
      await transaction.rollback();

      if (error instanceof HttpException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          message: 'Transfer between accounts error',
          error,
          sourceAccountId,
          targetAccountId,
          amount,
          currency
        });
      }
    }
  }

  /** Изменение баланса аккаунта */
  async updateAccountBalance(
    account_id: number,
    amount: number,
    currency: string,
  ) {
    const transaction = await this.sequelize.transaction();

    try {
      /**
       * Если сумма меньше нуля, значит это списание
       * В таком случае проверяем в этой же транзакции, что на счету достаточно средств
       */
      if (amount < 0) {
        const { balance } = await this.get(account_id, transaction);
  
        if (balance < amount) {
          throw new BadRequestException(ERRORS.ACCOUNT.NO_ENOUGH_MONEY.description);
        }
      }

      await this._updateBalance(account_id, amount, currency, transaction);

      await transaction.commit();
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      if (error instanceof HttpException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          message: 'Update account balance',
          error,
          account_id,
          amount,
          currency,
        });
      }

    }
  }

  /**
   * Изменение баланса аккаунта  
   * Приватный метод.  
   * Нужно вызывать из публичного метода с созданной транзакцией sequelize   
   */
  private async _updateBalance(
    account_id: number,
    amount: number,
    currency: string,
    transaction: any,
  ) {
    return Promise.all([
      /** Создание транзакции на обновление баланса */
      this._createBalanceTransaction(account_id, amount, currency, transaction),

      /** Обновление баланса аккаунта */
      this._updateAccountBalance(account_id, amount, transaction),
    ]);
  }

  /**
   * Создание транзакции для изменения баланса аккаунта  
   * Приватный метод.  
   * Нужно вызывать из публичного метода с созданной транзакцией sequelize  
   */
  private _createBalanceTransaction(
    account_id: number,
    amount: number,
    currency: string,
    transaction: any,
  ) {
    return this.transactionsModel.create({
      account_id,
      amount,
      currency,
    }, {
      transaction,
    });
  }

  /**
   * Обновление баланса аккаунта  
   * Приватный метод.  
   * Нужно вызывать из публичного метода с созданной транзакцией sequelize  
   */
  private _updateAccountBalance(
    account_id: number,
    amount: number,
    transaction: any, /** TODO Понять, где взять тип sequelize транзакции */
  ) {
    return this.accountsModel.update(
      {
        balance: this.sequelize.literal(`balance + ${amount}`)
      },
      {
        where: {
          account_id,
        },
        transaction,
      }
    )
  }
}