import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import config from '../config/database';
import * as repositories from './repositories';
import * as Models from './models';

@Module({
  imports: [
    SequelizeModule.forRoot({
      ...config,
      models: Object.values(Models),
    }),
    SequelizeModule.forFeature(Object.values(Models)),
  ],
  providers: Object.values(repositories),
  exports: Object.values(repositories),
})
export class DatabaseModule {}
