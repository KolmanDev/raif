import { ApiProperty } from "@nestjs/swagger";
import {
  IsPositive,
} from 'class-validator';
import { Type } from 'class-transformer';

export class TopupAccountBalanceRequestDto {
  @ApiProperty({
    type: Number,
    description: 'Сумма пополнения'
  })
  @IsPositive()
  @Type(() => Number)
    amount: number;
}