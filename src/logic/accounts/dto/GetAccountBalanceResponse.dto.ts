export class GetAccountBalanceResponse {
  /** Идентификатор аккаунта */
  account_id: number;
  
  /** Баланс аккаунта */
  balance: number;
}