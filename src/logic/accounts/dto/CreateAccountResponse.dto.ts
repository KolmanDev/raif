import { ApiProperty } from "@nestjs/swagger";
import {
  IsPositive,
  IsInt,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CreateAccountResponseDto {
  @ApiProperty({
    type: Number,
    description: 'Идентификатор аккаунта'
  })
  @IsInt()
  @IsPositive()
  @Type(() => Number)
    account_id: number;
}