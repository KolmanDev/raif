import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../app.module';
import { AccountsRepository } from '../../database/repositories';
import { TAccount } from 'src/database/models';

const accountRepositoryMock = {
  get: (): TAccount => ({ account_id: 1, balance: 10, currency: 'RUB' }),
}

describe('AccountController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AccountsRepository)
      .useValue(accountRepositoryMock)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/account/:account_id (GET)', () => {
    return request(app.getHttpServer())
      .get('/account/1')
      .expect(200)
      .expect({ account_id: 1, balance: 10, currency: 'RUB' });
  });
});
