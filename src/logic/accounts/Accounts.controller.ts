import { Controller, Get, HttpCode, HttpStatus, Param, Post, Body } from "@nestjs/common";
import { AccountsManager } from "./Accounts.manager";
import { ApiResponse, ApiBadRequestResponse } from "@nestjs/swagger";
import { GetAccountBalanceResponse } from "./dto/GetAccountBalanceResponse.dto";
import { TopupAccountBalanceRequestDto } from "./dto/TopupAccountBalanceRequest.dto";
import { CreateAccountResponseDto } from "./dto/CreateAccountResponse.dto";

@Controller('account')
export class AccountsController {
  constructor(
    private readonly accountsManager: AccountsManager,
  ) {}

  @ApiResponse({
    status: HttpStatus.OK,
    type: GetAccountBalanceResponse,
    description: 'Запрос баланса аккаунта',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Некорректные параметры запроса'
  })
  @HttpCode(HttpStatus.OK)
  @Get('/:account_id')
  getAccountBalance(
    @Param('account_id') account_id: number,
  ) {
    return this.accountsManager.getAccountBalance(account_id);
  }

  /** Пополнение баланса */
  @ApiResponse({
    status: HttpStatus.OK,
    type: TopupAccountBalanceRequestDto,
    description: 'Пополнение баланса',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Некорректные параметры запроса'
  })
  @HttpCode(HttpStatus.OK)
  @Post('/:account_id')
  topupAccountBalance(
    @Param('account_id') account_id: number,
    @Body() topupAccountBalanceDto: TopupAccountBalanceRequestDto,
  ) {
    return this.accountsManager.topupAccountBalance(
      account_id,
      topupAccountBalanceDto.amount,
    );
  }

  /** Создать счет */
  @ApiResponse({
    status: HttpStatus.OK,
    type: CreateAccountResponseDto,
    description: 'Создание счета'
  })
  @HttpCode(HttpStatus.OK)
  @Post('/')
  async createAccount() {
    const { account_id } = await this.accountsManager.createAccount();
    return { account_id };
  }
}