import { Inject, NotFoundException } from "@nestjs/common";
import { DEFAULT_CURRENCY } from "../../constants";
import { TAccount } from "../../database/models";
import { AccountsRepository } from "../../database/repositories";
import { LoggerService } from "../../services/logger/Logger.service";

export class AccountsManager {
  constructor(
    @Inject(AccountsRepository)
    private readonly accountsRepository: AccountsRepository,

    @Inject(LoggerService)
    private readonly logger: LoggerService,
  ) {}

  /** Запрос баланса аккаунта */
  async getAccountBalance(
    account_id: number,
  ): Promise<TAccount> {
    this.logger.log({ action: 'getAccountBalance', account_id });

    const account = await this.accountsRepository.get(account_id);

    if (!account) throw new NotFoundException('Аккаунт не найден');

    return account;
  }

  /** Создание аккаунта */
  createAccount(
    currency?: string,
  ): Promise<TAccount> {
    this.logger.log({ action: 'createAccount', currency });

    if (!currency) {
      currency = DEFAULT_CURRENCY;
    }

    return this.accountsRepository.create(currency);
  }

  /** Пополнение баланса аккаунта */
  async topupAccountBalance(
    account_id: number,
    amount: number,
    currency?: string,
  ) {
    this.logger.log({ action: 'topupAccountBalance', account_id, amount, currency });

    const account = await this.accountsRepository.get(account_id);

    if (!account) {
      throw new NotFoundException('Аккаунт не найден');
    }

    if (!currency) {
      currency = account.currency;
    }

    await this.accountsRepository.updateAccountBalance(account_id, amount, currency);
  }
}
