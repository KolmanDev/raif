import { Module } from "@nestjs/common";
import { DatabaseModule } from "../../database/DatabaseModule";
import { LoggerModule } from "../../services/logger/Logger.module";
import { AccountsController } from "./Accounts.controller";
import { AccountsManager } from "./Accounts.manager";

@Module({
  imports: [
    DatabaseModule,
    LoggerModule,
  ],
  controllers: [
    AccountsController,
  ],
  providers: [
    AccountsManager,
  ]
})
export class AccountsModule {}