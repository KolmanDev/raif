import { ApiProperty } from "@nestjs/swagger";
import {
  IsPositive,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CreateTransferRequestDto {
  @ApiProperty({
    type: Number,
    description: 'Сумма перевода'
  })
  @IsPositive()
  @Type(() => Number)
    amount: number;
}