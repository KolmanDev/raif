import { Controller, Post, HttpCode, Body, HttpStatus, Param } from "@nestjs/common";
import { TransferManager } from "./Transfer.manager";
import { ApiBadRequestResponse, ApiResponse } from "@nestjs/swagger";
import { CreateTransferRequestDto } from "./dto/createTransferRequest.dto";

@Controller('transfer')
export class TransferController {
  constructor(
    private readonly transferManager: TransferManager,
  ) {}

  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Перевод средств с одного счета на другой',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Некорректные данные запроса',
  })
  @HttpCode(HttpStatus.OK)
  @Post('/:sourceAccountId/:targetAccountId')

  createTransfer(
    @Param('sourceAccountId') sourceAccountId: number,
    @Param('targetAccountId') targetAccountId: number,
    @Body() createTransferDto: CreateTransferRequestDto,
  ) {
    return this.transferManager.transferBetweenAccounts(
      sourceAccountId,
      targetAccountId,
      createTransferDto.amount,
    );
  }
}