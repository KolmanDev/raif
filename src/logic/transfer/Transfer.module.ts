import { Module } from "@nestjs/common";
import { DatabaseModule } from "../../database/DatabaseModule";
import { TransferController } from "./Transfer.controller";
import { TransferManager } from "./Transfer.manager";
import { LoggerModule } from "../../services/logger/Logger.module";

@Module({
  imports: [
    DatabaseModule,
    LoggerModule
  ],
  controllers: [
    TransferController,
  ],
  providers: [
    TransferManager,
  ]
})
export class TransferModule {}