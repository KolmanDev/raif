import { Inject, NotFoundException, MethodNotAllowedException } from "@nestjs/common";
import { AccountsRepository } from "../../database/repositories";
import { LoggerService } from "../../services/logger/Logger.service";

export class TransferManager {
  constructor(
    @Inject(AccountsRepository)
    private readonly accountsRepository: AccountsRepository,

    @Inject(LoggerService)
    private readonly logger: LoggerService,
  ) {}

  /** Перевод средств с одного аккаунта на другой */
  async transferBetweenAccounts(
    sourceAccountId: number,
    targetAccountId: number,
    amount: number,
  ) {
    this.logger.log({ action: 'transferBetweenAccounts', sourceAccountId, targetAccountId, amount });
    
    const [
      sourceAccount,
      targetAccount
    ] = await Promise.all([
      this.accountsRepository.get(sourceAccountId),
      this.accountsRepository.get(targetAccountId),
    ]);

    /** Проверяем наличие аккаунтов */
    if (!sourceAccount || !targetAccount) {
      throw new NotFoundException('Указанные счета не найдены');
    }

    /** Проверяем, что валюты одинаковые */
    if (sourceAccount.currency !== targetAccount.currency) {
      throw new MethodNotAllowedException('Наш сервис еще не умеет конвертировать валюту');
    }

    /** Переводим средства */
    await this.accountsRepository.transferBetweenAccounts(
      sourceAccountId,
      targetAccountId,
      amount,
      sourceAccount.currency,
    );
  }
}