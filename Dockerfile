FROM node:16-alpine

RUN apk update && apk upgrade && \
    apk add --no-cache git

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

RUN npm ci

COPY . .

RUN npm run build

CMD ["sh", "-c", "node -v; npm -v; npm run migrate; npm run start;"]
